��          t      �                      &     ;     R     e          �  $   �     �  �  �     b  1   j     �  �   �     3  B   P     �     �  '   �     �         	                                           
        add calendar example calendar label input calendar tutorial link calendar url input calendar_week_description calendar_week_title invalid calendar url max calendar week categories exeeded required Project-Id-Version: mirr.OS v0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-17 10:20+0100
PO-Revision-Date: 2018-09-04 15:15+0200
Last-Translator: gorbo <info@gorbo.de>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.1.1
Language: fr_FR
 ajouter (ex.: iCal, Google Calendar, Mozilla Thunderbird) lettrage Ici, vous apprenez où trouver votre <a href="https://glancr.de/tutorials/das-kalendermodul-konfigurieren/" target="_blank">Calendarfeed</a>. Insérer l'URL du calendrier Jusqu'à 5 flux de calendrier dans la vue d'ensemble hebdomadaire. Résumé hebdomadaire URL de calendrier invalide Nombre maximum de catégories atteintes champ obligatoire 